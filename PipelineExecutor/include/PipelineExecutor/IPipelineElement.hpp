#pragma once

#include "ModulesManager/RegisterInterface.hpp"

namespace PipelineExecutor {

class IPipelineElement {
public: // methods
    virtual ~IPipelineElement() = default;

    virtual void iterate() = 0;
};

}

DP_REGISTER_INTERFACE(PipelineExecutor::IPipelineElement*)