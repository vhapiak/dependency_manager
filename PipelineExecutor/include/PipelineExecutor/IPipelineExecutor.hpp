#pragma once

#include <memory>
#include <string>
#include <vector>

#include "ModesManager/IModesManager.hpp"
#include "ModulesManager/IModulesManager.hpp"

namespace PipelineExecutor {

class IPipelineExecutor
{
public: // types
    struct Config
    {
        ModesManager::IModesManager::Config modules;
        std::vector<std::string> execution_order;
    };

public: // methods
    virtual ~IPipelineExecutor() = default;

    virtual void iterate() = 0;
    virtual void changeMode(std::string const& mode) = 0;
};

using PipelineExecutorPtr = std::shared_ptr<IPipelineExecutor>;

PipelineExecutorPtr createInstance(
    IPipelineExecutor::Config const& config,
    ModulesManager::ModulesManagerPtr modules_manager);

} // namespace PipelineExecutor