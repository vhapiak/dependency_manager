
#include "ModesManager/IModesManager.hpp"
#include "PipelineExecutor/IPipelineElement.hpp"
#include "PipelineExecutor/IPipelineExecutor.hpp"

namespace PipelineExecutor {

class CPipelineExecutor : public IPipelineExecutor
{
public: // methods
    explicit CPipelineExecutor(
        IPipelineExecutor::Config const& config,
        ModulesManager::ModulesManagerPtr modules_manager)
        : m_execution_order{config.execution_order}
        , m_modules_manager{modules_manager}
        , m_modes_manager{ModesManager::createInstance(config.modules, modules_manager)}
    {
    }

    void iterate() override
    {
        for (auto* element : m_active_elements)
        {
            element->iterate();
        }
    }

    void changeMode(std::string const& mode) override
    {
        m_modes_manager->changeMode(mode);

        auto active_elements = m_modes_manager->getActiveElements();
        m_active_elements.clear();
        for (auto& element : m_execution_order)
        {
            auto const it = active_elements.find(element);
            if (it != active_elements.end())
            {
                auto interface = m_modules_manager->getInterface<PipelineExecutor::IPipelineElement*>(
                    it->second + "." + it->first + "." + "pipeline");
                m_active_elements.push_back(interface);
            }
        }
    }

private:
    std::vector<std::string> m_execution_order;
    ModulesManager::ModulesManagerPtr m_modules_manager;
    ModesManager::ModesManagerPtr m_modes_manager;

    std::vector<IPipelineElement*> m_active_elements;
};

PipelineExecutorPtr createInstance(
    IPipelineExecutor::Config const& config,
    ModulesManager::ModulesManagerPtr modules_manager)
{
    return std::make_shared<CPipelineExecutor>(config, modules_manager);
}

} // namespace PipelineExecutor