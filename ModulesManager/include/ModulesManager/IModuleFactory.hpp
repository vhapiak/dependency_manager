#pragma once

#include <map>
#include <memory>
#include <string>

#include "ModulesManager/IModule.hpp"

namespace ModulesManager {

class IModuleFactory;
using ModuleFactoryUPtr = std::unique_ptr<IModuleFactory>;
using ModuleFactoryPtr = std::shared_ptr<IModuleFactory>;

class IModuleFactory
{
public: // types
    struct InterfaceData {
        std::string type; // fully qualified interface type name
        bool is_deferred;
    };
    using NameToInterfaceInfo = std::map<std::string, InterfaceData>;
    using NameToSubmodule = std::map<std::string, ModuleFactoryPtr>;

public: // methods
    virtual ~IModuleFactory() = default;

    virtual NameToSubmodule getSubmodules() const = 0;

    // what is required by module
    virtual NameToInterfaceInfo getRequiredInterfaces() = 0;

    // what will be provided by module
    virtual NameToInterfaceInfo getProvidableInterfaces() = 0;

    virtual ModuleUPtr createInstance(IModule::NameToInterface const& dependencies) = 0;
};


} // namespace ModulesManager