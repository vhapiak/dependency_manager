#pragma once

#include <memory>
#include <string>
#include <vector>

#include "ModulesManager/IModuleFactory.hpp"
#include "ModulesManager/RegisterInterface.hpp"
#include "ModulesManager/TProvidedIface.hpp"

namespace ModulesManager {

class IModulesManager
{
public: // types
    using InstanceId = std::uint64_t;

    ///
    /// Key: module dependency name
    /// Value:
    ///
    using DependencyHints = std::map<std::string, std::string>;

    struct InstanceData
    {
        std::string module;
        std::string name;
        DependencyHints dependency_hints;
    };

public: // methods
    virtual ~IModulesManager() = default;

    virtual void registerModule(std::string const& name, ModuleFactoryPtr factory) = 0;

    virtual InstanceId createModuleInstance(
        std::string const& module,
        std::string const& name,
        DependencyHints const& dependency_hints = {}) = 0;

    // this cannot be done with RAII because this operation can be invalid
    // if specified instace provides some interface needed by still existed modules
    virtual void removeModuleInstance(InstanceId id) = 0;

    virtual std::vector<InstanceData const*> sort(std::vector<InstanceData> const& instances) = 0;

    template <typename T>
    T getInterface(std::string const& instance) const;

protected:
    virtual IProvidedIface& getGenericInterface(std::string const& instance) const = 0;
};

using ModulesManagerPtr = std::shared_ptr<IModulesManager>;

ModulesManagerPtr createInstance();


/// IMPL

template <typename T>
T IModulesManager::getInterface(std::string const& instance) const
{
    auto& interface = getGenericInterface(instance); 
    auto& specific_interface = dynamic_cast<TProvidedIface<T>&>(interface);
    return specific_interface.getImpl();
}

} // namespace ModulesManager

DP_REGISTER_INTERFACE(ModulesManager::IModulesManager*);