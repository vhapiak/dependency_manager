#pragma once

#define DP_REGISTER_INTERFACE(TYPE)                     \
    namespace ModulesManager {                       \
    template <>                                         \
    struct InterfaceInfo<TYPE>                          \
    {                                                   \
        static char const* getType() { return #TYPE; } \
    };                                                  \
    }

namespace ModulesManager {

template <typename T>
struct InterfaceInfo
{
    static char const* getType();
};

} // namespace ModulesManager