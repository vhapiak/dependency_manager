#pragma once

#include <tuple>

#include "ModulesManager/IModule.hpp"
#include "ModulesManager/IModuleFactory.hpp"
#include "ModulesManager/RegisterInterface.hpp"
#include "ModulesManager/TProvidedIface.hpp"

namespace ModulesManager {

template <typename... InterfacesDesc>
class TProvides
{
public:
    static IModuleFactory::NameToInterfaceInfo getProvidableInterfaces()
    {
        return {makePairInfo<InterfacesDesc>()...};
    }

    template <typename T>
    TProvidedIface<typename T::Type>& getProvidedIface()
    {
        return std::get<T>(m_provided).instance;
    }

    IModule::NameToInterface getProvidedInterfaces() { return {makePair<InterfacesDesc>()...}; }

private:
    template <typename InterfaceDesc>
    static std::pair<std::string, IModuleFactory::InterfaceData> makePairInfo()
    {
        return {
            InterfaceDesc::getName(),
            IModuleFactory::InterfaceData{
                InterfaceInfo<typename InterfaceDesc::Type>::getType(), false}};
    }

    template <typename InterfaceDesc>
    std::pair<std::string, IProvidedIface*> makePair()
    {
        return {InterfaceDesc::getName(), &getProvidedIface<InterfaceDesc>()};
    }

    std::tuple<InterfacesDesc...> m_provided;
};

} // namespace ModulesManager