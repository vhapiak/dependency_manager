#pragma once

#include "ModulesManager/DefineModule.hpp"
#include "ModulesManager/IModuleFactory.hpp"
#include "ModulesManager/TModule.hpp"
#include "ModulesManager/TProvidedIface.hpp"
#include "ModulesManager/TProvides.hpp"
#include "ModulesManager/TRequires.hpp"

namespace ModulesManager {

template <typename T>
class TModuleFactory : public IModuleFactory
{
private: // types
    using Requires = typename T::Requires;
    using Provides = typename T::Provides;
    using Module = TModule<T, Requires, Provides>;

public:
    explicit TModuleFactory(NameToSubmodule submodules)
        : m_submodules{std::move(submodules)}
    {
    }

    NameToSubmodule getSubmodules() const override { return m_submodules; }

    NameToInterfaceInfo getRequiredInterfaces() override
    {
        return Requires::getRequiredInterfaces();
    }

    NameToInterfaceInfo getProvidableInterfaces() override
    {
        return Provides::getProvidableInterfaces();
    }

    ModuleUPtr createInstance(IModule::NameToInterface const& dependencies) override
    {
        return std::make_unique<Module>(dependencies);
    }

private: // fields
    NameToSubmodule m_submodules;
};

template <typename T>
std::shared_ptr<TModuleFactory<T>> makeModuleFactory(IModuleFactory::NameToSubmodule submodules = {})
{
    return std::make_shared<TModuleFactory<T>>(std::move(submodules));
}

} // namespace ModulesManager