#pragma once

#include "ModulesManager/IProvidedIface.hpp"
#include "ModulesManager/RegisterInterface.hpp"

namespace ModulesManager {

template <typename T>
class TProvidedIface : public IProvidedIface
{
public:
    explicit TProvidedIface()
        : m_initialized{false}
        , m_captures{0}
    {
    }

    void setImpl(T instance)
    {
        m_instance = instance;
        m_initialized = true;
    }

    T getImpl() { return m_instance; }

    std::string getInterfaceType() const override { return InterfaceInfo<T>::getType(); }

    bool isInitialized() const override { return m_initialized; }

    void capture() override { ++m_captures; }
    void release() override { --m_captures; }
    std::size_t amountOfCaptures() override { return m_captures; }

private:
    bool m_initialized;
    T m_instance;
    std::size_t m_captures;
};

} // namespace ModulesManager