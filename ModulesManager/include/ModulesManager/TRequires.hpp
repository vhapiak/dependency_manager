#pragma once

#include <stdexcept>
#include <tuple>
#include <type_traits>

#include "ModulesManager/IDeferredIface.hpp"
#include "ModulesManager/IModule.hpp"
#include "ModulesManager/TDeferredIface.hpp"
#include "ModulesManager/TProvidedIface.hpp"

namespace ModulesManager {

template <typename... InterfacesDesc>
class TRequires
{
public:
    static IModuleFactory::NameToInterfaceInfo getRequiredInterfaces()
    {
        return {makePairInfo<InterfacesDesc>()...};
    }

    TRequires(IModule::NameToInterface const& dependencies)
        : m_interfaces{InterfacesDesc{extractDependency<InterfacesDesc>(dependencies)}...}
    {
    }

    template <typename T>
    typename T::ArgType getRequiredInterface()
    {
        return this->extractInstance(std::get<T>(m_interfaces).instance);
    }

    IModule::NameToDeferred getDeferredDependencies()
    {
        IModule::NameToDeferred results;
        int tmp[] = {(fillDeferred<InterfacesDesc>(results), 0)...};
        return results;
    }

private:
    template <typename InterfaceDesc>
    static std::pair<std::string, IModuleFactory::InterfaceData> makePairInfo()
    {
        const auto is_deferred =
            std::is_base_of<IDeferredIface, typename InterfaceDesc::Instance>::value;
        return {
            InterfaceDesc::getName(),
            IModuleFactory::InterfaceData{
                InterfaceInfo<typename InterfaceDesc::Type>::getType(), is_deferred}};
    }

    template <typename InterfaceDesc>
    static TProvidedIface<typename InterfaceDesc::Type>* extractDependency(
        IModule::NameToInterface const& dependencies)
    {
        auto const it = dependencies.find(InterfaceDesc::getName());
        if (it == dependencies.end())
        {
            auto const is_deferred =
                std::is_base_of<IDeferredIface, typename InterfaceDesc::Instance>::value;
            if (is_deferred)
            {
                return nullptr;
            }
            throw std::runtime_error{std::string{"Missed dependency: "} + InterfaceDesc::getName()};
        }

        /// @todo check interface info and use static cast
        return dynamic_cast<TProvidedIface<typename InterfaceDesc::Type>*>(it->second);
    }

    template <typename T>
    static T extractInstance(TProvidedIface<T>* instance)
    {
        return instance->getImpl();
    }

    template <typename T>
    static TDeferredIface<T>& extractInstance(TDeferredIface<T>& instance)
    {
        return instance;
    }

    template <typename InterfaceDesc>
    void fillDeferred(IModule::NameToDeferred& map)
    {
        fillDeferred(InterfaceDesc::getName(), map, getRequiredInterface<InterfaceDesc>());
    }

    template <typename T>
    void fillDeferred(char const* name, IModule::NameToDeferred& map, TDeferredIface<T>& deferred)
    {
        if (!deferred.isInitialized())
        {
            map.emplace(name, &deferred);
        }
    }

    template <typename T>
    void fillDeferred(char const* name, IModule::NameToDeferred& map, T)
    {
    }

    std::tuple<InterfacesDesc...> m_interfaces;
};

} // namespace ModulesManager