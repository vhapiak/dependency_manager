#pragma once

#include <functional>

#include "ModulesManager/IDeferredIface.hpp"
#include "ModulesManager/RegisterInterface.hpp"
#include "ModulesManager/TProvidedIface.hpp"

namespace ModulesManager {

template <typename T>
class TDeferredIface : public IDeferredIface
{
public: // types
    using AvailableCallback = std::function<void(T)>;
    using UnavailableCallback = std::function<void()>;

public: // methods
    TDeferredIface(TProvidedIface<T>* delegate)
        : m_initialized{delegate != nullptr}
        , m_instance{}
    {
        if (m_initialized)
        {
            m_instance = delegate->getImpl();
        }
    }

    void onAvailable(AvailableCallback callback)
    {
        m_available_callback = callback;
        if (m_initialized)
        {
            m_available_callback(m_instance);
        }
    }

    void onUnavailable(UnavailableCallback callback)
    {
        m_unavailable_callback = callback;
        if (!m_initialized)
        {
            m_unavailable_callback();
        }
    }

    std::string getInterfaceType() const override { return InterfaceInfo<T>::getType(); }

    bool isInitialized() const override { return m_initialized; }

    void setInterface(IProvidedIface* interface) override
    {
        m_initialized = true;
        m_instance = dynamic_cast<TProvidedIface<T>*>(interface)->getImpl();
        if (m_available_callback)
        {
            m_available_callback(m_instance);
        }
    }

    void resetInterface() override
    {
        m_initialized = false;
        if (m_unavailable_callback)
        {
            m_unavailable_callback();
        }
    }

private: // fields
    bool m_initialized;
    T m_instance;
    AvailableCallback m_available_callback;
    UnavailableCallback m_unavailable_callback;
};

} // namespace ModulesManager