#pragma once

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/if.hpp>
#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/size.hpp>
#include <boost/preprocessor/seq/transform.hpp>

#include "ModulesManager/TDeferredIface.hpp"
#include "ModulesManager/TProvidedIface.hpp"

#define DP_DO_NOTHING(...)
#define DP_COMA(...) ,

// the next macros convert sequence of (a, b)(c, d, e) to ((a, b, NORMAL))((c, d, e, NORMAL))
#define DP_DOUBLE_WRAP_0(...) ((__VA_ARGS__, DEFAULT)) DP_DOUBLE_WRAP_1
#define DP_DOUBLE_WRAP_1(...) ((__VA_ARGS__, DEFAULT)) DP_DOUBLE_WRAP_0
#define DP_DOUBLE_WRAP_0_END
#define DP_DOUBLE_WRAP_1_END

#define DP_GEN_STRUCT_NAME(TYPE, NAME, ...) c_##NAME
#define DP_GEN_STRUCT_NAME_PROXY(R, DATA, PAIR) DP_GEN_STRUCT_NAME PAIR

#define DP_DEFINE_REQ_DEFAULT(TYPE, NAME)                            \
    struct DP_GEN_STRUCT_NAME(TYPE, NAME)                            \
    {                                                                \
        using ArgType = TYPE;                                        \
        using Type = TYPE;                                           \
        using Instance = ::ModulesManager::TProvidedIface<Type>*; \
        static char const* getName() { return #NAME; }               \
        Instance instance = nullptr;                                 \
    };

#define DP_DEFINE_REQ_DEFERRED(TYPE, NAME)                          \
    struct DP_GEN_STRUCT_NAME(TYPE, NAME)                           \
    {                                                               \
        using Type = TYPE;                                          \
        using Instance = ::ModulesManager::TDeferredIface<Type>; \
        using ArgType = Instance&;                                  \
        static char const* getName() { return #NAME; }              \
        Instance instance;                                          \
    };

#define DP_DEFINES_REQ_STRUCT(TYPE, NAME, MODE, ...) DP_DEFINE_REQ_##MODE(TYPE, NAME)
#define DP_DEFINES_REQ_STRUCT_PROXY(R, DATA, PAIR) DP_DEFINES_REQ_STRUCT PAIR

#define DP_DEFINES_PVD_STRUCT(TYPE, NAME, MODE, ...)        \
    struct DP_GEN_STRUCT_NAME(TYPE, NAME)                   \
    {                                                       \
        using Type = TYPE;                                  \
        static char const* getName() { return #NAME; }      \
        ::ModulesManager::TProvidedIface<Type> instance; \
    };
#define DP_DEFINES_PVD_STRUCT_PROXY(R, DATA, PAIR) DP_DEFINES_PVD_STRUCT PAIR

#define DP_GEN_REQ_ARG(TYPE, NAME, MODE, ...) DP_GEN_STRUCT_NAME(TYPE, NAME)::ArgType NAME
#define DP_GEN_REQ_ARG_PROXY(R, DATA, PAIR) DP_GEN_REQ_ARG PAIR

#define DP_GEN_PRV_ARG(TYPE, NAME, MODE, ...) ModulesManager::TProvidedIface<TYPE>& NAME
#define DP_GEN_PRV_ARG_PROXY(R, DATA, PAIR) DP_GEN_PRV_ARG PAIR

#define DP_JOIN_NAMES(SEQ, DATA) \
    BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(DP_GEN_STRUCT_NAME_PROXY, ~, SEQ))
#define DP_JOIN_REQUIRES(SEQ, DATA) \
    BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(DP_GEN_REQ_ARG_PROXY, ~, SEQ))
#define DP_JOIN_PROVIDES(SEQ, DATA)                              \
    BOOST_PP_IF(BOOST_PP_SEQ_SIZE(DATA), DP_COMA, DP_DO_NOTHING) \
    (, ) BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(DP_GEN_PRV_ARG_PROXY, ~, SEQ))

#define DP_EXEC_NOT_EMPTY(OP, SEQ, DATA) \
    BOOST_PP_IF(BOOST_PP_SEQ_SIZE(SEQ), OP, DP_DO_NOTHING)(SEQ, DATA)

#define DP_DEFINE_MODULE_WRAPPED(NAME, REQUIRES, PROVIDES)                                        \
    BOOST_PP_SEQ_FOR_EACH(DP_DEFINES_REQ_STRUCT_PROXY, ~, REQUIRES)                               \
    using Requires = ModulesManager::TRequires<DP_EXEC_NOT_EMPTY(DP_JOIN_NAMES, REQUIRES, ~)>; \
    BOOST_PP_SEQ_FOR_EACH(DP_DEFINES_PVD_STRUCT_PROXY, ~, PROVIDES)                               \
    using Provides = ModulesManager::TProvides<DP_EXEC_NOT_EMPTY(DP_JOIN_NAMES, PROVIDES, ~)>; \
    NAME(DP_EXEC_NOT_EMPTY(DP_JOIN_REQUIRES, REQUIRES, ~)                                         \
             DP_EXEC_NOT_EMPTY(DP_JOIN_PROVIDES, PROVIDES, REQUIRES))

#define DP_DEFINE_MODULE_PROXY(NAME, REQUIRES, PROVIDES) \
    DP_DEFINE_MODULE_WRAPPED(                            \
        NAME,                                            \
        BOOST_PP_CAT(DP_DOUBLE_WRAP_0 REQUIRES, _END),   \
                                                         \
        BOOST_PP_CAT(DP_DOUBLE_WRAP_0 PROVIDES, _END))

#define DP_REQUIRES(...) __VA_ARGS__
#define DP_PROVIDES(...) __VA_ARGS__

#define DP_DEFINE_MODULE(NAME, REQUIRES, PROVIDES) \
    DP_DEFINE_MODULE_PROXY(NAME, DP_##REQUIRES, DP_##PROVIDES)