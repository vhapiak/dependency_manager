#pragma once

#include "ModulesManager/IModule.hpp"
#include "ModulesManager/TProvides.hpp"
#include "ModulesManager/TRequires.hpp"

namespace ModulesManager {

template <typename... T>
class TModule;

template <typename T, typename... RequiresItems, typename... ProvidesItems>
class TModule<T, TRequires<RequiresItems...>, TProvides<ProvidesItems...>> : public IModule
{
public: // types
    using Requires = TRequires<RequiresItems...>;
    using Provides = TProvides<ProvidesItems...>;

public: // methods
    TModule(NameToInterface const& dependencies)
        : m_required_interfaces{dependencies}
        , m_provided_interfaces{}
        , m_instance{
              m_required_interfaces.template getRequiredInterface<RequiresItems>()...,
              m_provided_interfaces.template getProvidedIface<ProvidesItems>()...}
    {
    }

    NameToInterface getProvidedInterfaces() override
    {
        return m_provided_interfaces.getProvidedInterfaces();
    }

    NameToDeferred getDeferredDependencies() override 
    {
        return m_required_interfaces.getDeferredDependencies();
    }

private: // fields
    Requires m_required_interfaces;
    Provides m_provided_interfaces;
    T m_instance;
};

} // namespace ModulesManager