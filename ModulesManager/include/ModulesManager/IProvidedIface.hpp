#pragma once

#include <string>

namespace ModulesManager {

class IProvidedIface {
public:
    virtual ~IProvidedIface() = default;

    virtual std::string getInterfaceType() const = 0;
    virtual bool isInitialized() const = 0;

    virtual void capture() = 0;
    virtual void release() = 0;
    virtual std::size_t amountOfCaptures() = 0;
};

}