#pragma once

#include <string>

#include "ModulesManager/IProvidedIface.hpp"

namespace ModulesManager {

class IDeferredIface {
public:
    virtual ~IDeferredIface() = default;

    virtual std::string getInterfaceType() const = 0;
    virtual bool isInitialized() const = 0;

    virtual void setInterface(IProvidedIface* interface) = 0;
    virtual void resetInterface() = 0;
};

}