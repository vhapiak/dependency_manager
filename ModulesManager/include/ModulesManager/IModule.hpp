#pragma once

#include <functional>
#include <map>
#include <memory>
#include <string>

#include "ModulesManager/IProvidedIface.hpp"
#include "ModulesManager/IDeferredIface.hpp"

namespace ModulesManager {

class IModule
{
public: // types
    using NameToInterface = std::map<std::string, IProvidedIface*>;
    using NameToDeferred = std::map<std::string, IDeferredIface*>;

public: // methods
    virtual ~IModule() = default;

    virtual NameToInterface getProvidedInterfaces() = 0;
    virtual NameToDeferred getDeferredDependencies() = 0;
};

using ModuleUPtr = std::unique_ptr<IModule>;

} // namespace ModulesManager