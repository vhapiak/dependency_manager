
#include <map>
#include <set>
#include <stdexcept>
#include <vector>

#include "Logger/Logger.hpp"

#include "ModulesManager/IModulesManager.hpp"
#include "ModulesManager/TDeferredIface.hpp"

namespace ModulesManager {

class CModulesManager : public IModulesManager
{
public:
    void registerModule(std::string const& name, ModuleFactoryPtr factory) override
    {
        LOG_DBG << "CModulesManager::registerModule() " << name;

        LOG_DBG << "  Requires:";
        for (auto& pair : factory->getRequiredInterfaces())
        {
            LOG_DBG << "    " << pair.first << ": " << pair.second.type << std::boolalpha
                    << " is_deferred: " << pair.second.is_deferred;
        }

        LOG_DBG << "  Provides:";
        for (auto& pair : factory->getProvidableInterfaces())
        {
            LOG_DBG << "    " << pair.first << ": " << pair.second.type;
        }

        LOG_DBG << "  Submodules:";
        for (auto& pair : factory->getSubmodules())
        {
            LOG_DBG << "    <" << pair.first;
            registerModule(name + "." + pair.first, pair.second);
            LOG_DBG << "    >" << pair.first;
        }

        m_modules.emplace(name, std::move(factory));
    }

    InstanceId createModuleInstance(
        std::string const& module,
        std::string const& name,
        DependencyHints const& dependency_hints) override
    {
        auto const it = m_modules.find(module);
        if (it == m_modules.end())
        {
            throw std::runtime_error{"No such module: " + module};
        }
        InstanceData instance_data{module, name, nullptr, {}};

        LOG_DBG << "CModulesManager::createModuleInstance() " << module << '.' << name;
        LOG_DBG << "  Resolving dependencies...";
        instance_data.dependencies =
            resolveDependencies(it->second, dependency_hints, {"modules_manager"});

        LOG_DBG << "  Creating...";
        instance_data.instance = it->second->createInstance(instance_data.dependencies);
        LOG_DBG << "  Created. Provided interfaces:";

        // capture all interfaces after successful instance creation
        for (auto& pair : instance_data.dependencies)
        {
            pair.second->capture();
        }

        for (auto& pair : instance_data.instance->getProvidedInterfaces())
        {
            if (!pair.second->isInitialized())
            {
                throw std::runtime_error{
                    "Interface is not initialized: " + pair.first + "(" +
                    pair.second->getInterfaceType() + ")"};
            }
            LOG_DBG << "    " << pair.first << "(" << pair.second->getInterfaceType() << ")";
        }

        // now we know that everything is okay
        for (auto& pair : instance_data.instance->getProvidedInterfaces())
        {
            auto type = pair.second->getInterfaceType();
            auto interface_name = genInstanceName(module, name, pair.first);
            m_interfaces_by_instance.emplace(interface_name, pair.second);

            auto& interface_data = m_interfaces_by_type[type];
            interface_data.providers.emplace(interface_name, pair.second);
            if (interface_data.providers.size() > 1 && !interface_data.waiters.empty())
            {
                throw std::runtime_error{"Ambiguous deferred dependency"};
            }
            for (auto& waiter : interface_data.waiters)
            {
                waiter->setInterface(pair.second);
            }
            auto const exact_waiters_it = interface_data.waiters_exact_providers.find(interface_name);
            if (exact_waiters_it != interface_data.waiters_exact_providers.end())
            {
                for (auto& waiter : exact_waiters_it->second)
                {
                    waiter->setInterface(pair.second);
                }
            }
        }

        LOG_DBG << "  Deferred dependencies:";
        for (auto& pair : instance_data.instance->getDeferredDependencies())
        {
            auto const type = pair.second->getInterfaceType();
            LOG_DBG << "    " << pair.first << "(" << type << ")";

            auto& interface_data = m_interfaces_by_type[type];
            auto const hint_it = dependency_hints.find(pair.first);
            if (hint_it == dependency_hints.end())
            {
                interface_data.waiters.push_back(pair.second);
            }
            else
            {
                interface_data.waiters_exact_providers[hint_it->second].push_back(pair.second);
            }
        }

        auto id = m_next_id++;
        LOG_DBG << "CModulesManager::createModuleInstance() id: " << id;

        m_instances.emplace(id, std::move(instance_data));

        LOG_DBG << "CModulesManager::createModuleInstance() created";

        return id;
    }

    void removeModuleInstance(InstanceId id) override
    {
        LOG_DBG << "CModulesManager::removeModuleInstance() " << id;
        auto instance_it = m_instances.find(id);
        if (instance_it == m_instances.end())
        {
            throw std::runtime_error{"There is no such instance: " + std::to_string(id)};
        }

        auto& data = instance_it->second;
        for (auto& pair : data.instance->getProvidedInterfaces())
        {
            if (pair.second->amountOfCaptures() != 0)
            {
                throw std::runtime_error{
                    "Provided interface is needed by another instance: " + pair.first};
            }
        }

        LOG_DBG << "  Removing provided interfaces:";
        for (auto& pair : data.instance->getProvidedInterfaces())
        {
            auto interface_name = genInstanceName(data.module, data.name, pair.first);
            auto type = pair.second->getInterfaceType();
            m_interfaces_by_instance.erase(interface_name);
            auto& interface_data = m_interfaces_by_type[type];
            interface_data.providers.erase(interface_name);
            if (interface_data.providers.empty())
            {
                for (auto& waiter : interface_data.waiters)
                {
                    waiter->resetInterface();
                }
            }
            auto const exact_waiters_it = interface_data.waiters_exact_providers.find(interface_name);
            if (exact_waiters_it != interface_data.waiters_exact_providers.end())
            {
                for (auto& waiter : exact_waiters_it->second)
                {
                    waiter->resetInterface();
                }
            }
            LOG_DBG << "    " << interface_name << "(" << type << ")";
        }

        LOG_DBG << "  Releasing dependencies";
        for (auto& pair : data.dependencies)
        {
            LOG_DBG << "    " << pair.first << "(" << pair.second->getInterfaceType() << ")";
            pair.second->release();
        }

        m_instances.erase(instance_it);
        LOG_DBG << "CModulesManager::removeModuleInstance() removed";
    }

    IModule::NameToInterface resolveDependencies(
        ModuleFactoryPtr const& module,
        DependencyHints const& dependency_hints,
        std::set<std::string> const& ignore)
    {
        IModule::NameToInterface dependencies;
        for (auto& pair : module->getRequiredInterfaces())
        {
            if (ignore.count(pair.first) > 0)
            {
                continue;
            }

            auto interface = findInterface(pair.first, pair.second.type, dependency_hints);
            if (interface.first != nullptr)
            {
                dependencies.emplace(pair.first, interface.first);
            }
            else if (!pair.second.is_deferred)
            {
                throw std::runtime_error{interface.second};
            }
        }
        return dependencies;
    }

    std::vector<InstanceData const*> sort(std::vector<InstanceData> const& instances) override
    {
        using InstanceData = IModulesManager::InstanceData;
        InstanceData system_ifaces{"<system>", "_", {}};

        std::map<std::string, InstanceData const*> by_types;
        for (auto& pair : m_interfaces_by_type)
        {
            by_types.emplace(pair.first, &system_ifaces);
        }
        std::map<std::string, InstanceData const*> by_hints;
        for (auto& pair : m_interfaces_by_instance)
        {
            by_hints.emplace(pair.first, &system_ifaces);
        }

        for (auto const& instance : instances)
        {
            auto const it = m_modules.find(instance.module);
            if (it == m_modules.end())
            {
                throw std::runtime_error{"No such module: " + instance.module};
            }
            auto module = it->second;
            for (auto& pair : module->getProvidableInterfaces())
            {
                auto name = genInstanceName(instance.module, instance.name, pair.first);
                by_hints.emplace(name, &instance);
                by_types.emplace(pair.second.type, &instance);
            }
        }

        std::vector<InstanceData const*> order;
        std::set<InstanceData const*> visited;
        visited.insert(&system_ifaces);

        std::function<void(InstanceData const&)> dfs =
            [&dfs, &by_types, &by_hints, &order, &visited, this](InstanceData const& instance) {
                if (visited.count(&instance))
                {
                    return;
                }
                /// @todo detect cycles
                visited.emplace(&instance);

                auto const it = m_modules.find(instance.module);
                if (it == m_modules.end())
                {
                    throw std::runtime_error{"No such module: " + instance.module};
                }
                auto module = it->second;
                for (auto& pair : module->getRequiredInterfaces())
                {
                    if (!pair.second.is_deferred)
                    {
                        auto const hint_it = instance.dependency_hints.find(pair.first);
                        if (hint_it != instance.dependency_hints.end())
                        {
                            auto const& hint = hint_it->second;
                            auto const by_hint_it = by_hints.find(hint);
                            if (by_hint_it == by_hints.end())
                            {
                                throw std::runtime_error{"Cannot find " + hint};
                            }
                            dfs(*(by_hint_it->second));
                        }
                        else
                        {
                            auto const by_type_it = by_types.find(pair.second.type);
                            if (by_type_it == by_types.end())
                            {
                                throw std::runtime_error{"Cannot find " + pair.second.type};
                            }
                            dfs(*(by_type_it->second));
                        }
                    }
                }
                order.push_back(&instance);
            };

        for (auto const& instance : instances)
        {
            dfs(instance);
        }

        return order;
    }

    IProvidedIface& getGenericInterface(std::string const& instance) const override
    {
        return *m_interfaces_by_instance.at(instance);
    }

private: // types
    struct InstanceData
    {
        std::string module;
        std::string name;
        ModuleUPtr instance;
        IModule::NameToInterface dependencies;
    };

    using InterfaceByInstance = std::map<std::string, IProvidedIface*>;

    using Waiters = std::vector<IDeferredIface*>;
    struct InterfaceData
    {
        InterfaceByInstance providers;
        Waiters waiters;
        std::map<std::string, Waiters> waiters_exact_providers;
    };

    using InterfaceByType = std::map<std::string, InterfaceData>;

private: // methods
    std::pair<IProvidedIface*, std::string> findInterface(
        std::string const& name,
        std::string const& type,
        DependencyHints const& dependency_hints)
    {
        auto hint_it = dependency_hints.find(name);
        if (hint_it != dependency_hints.end())
        {
            auto interface_it = m_interfaces_by_instance.find(hint_it->second);
            if (interface_it == m_interfaces_by_instance.end())
            {
                return std::make_pair(nullptr, "Cannot find interface by hint: " + hint_it->second);
            }
            return std::make_pair(interface_it->second, "");
        }

        // try to find by type
        auto const set = m_interfaces_by_type.find(type);
        if (set == m_interfaces_by_type.end() || set->second.providers.empty())
        {
            return std::make_pair(nullptr, "Interface did not provided: " + type);
        }
        else if (set->second.providers.size() > 1)
        {
            return std::make_pair(nullptr, "Interface selection is ambiguous: " + type);
        }
        return std::make_pair(set->second.providers.begin()->second, "");
    }

    std::string genInstanceName(std::string const& module, std::string const& name, std::string const& interface)
    {
        return module + "." + name + "." + interface;
    }

private: // fields
    std::map<std::string, ModuleFactoryPtr> m_modules;
    InstanceId m_next_id = 0;
    std::map<InstanceId, InstanceData> m_instances;
    InterfaceByType m_interfaces_by_type;
    InterfaceByInstance m_interfaces_by_instance;
};

ModulesManagerPtr createInstance()
{
    return std::make_shared<CModulesManager>();
}

} // namespace ModulesManager