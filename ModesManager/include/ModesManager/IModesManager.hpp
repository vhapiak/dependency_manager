#pragma once

#include <memory>
#include <set>
#include <string>
#include <vector>

#include "ModulesManager/IModulesManager.hpp"

namespace ModesManager {

class IModesManager
{
public: // types
    struct SubElementConfig
    {
        std::string module;
        std::set<std::string> modes;
    };

    struct ElementConfig
    {
        std::string module;
        std::set<std::string> modes;
        std::map<std::string, SubElementConfig> subelements; // by name
    };

    struct Config
    {
        std::set<std::string> modes;
        std::map<std::string, ElementConfig> elements; // by name
    };

    struct ModuleInstance
    {
        std::string name;
        std::string module;
    };

public: // methods
    virtual ~IModesManager() = default;

    virtual void changeMode(std::string const& mode) = 0;

    /// @todo update signature
    virtual std::map<std::string, std::string> getActiveElements() const = 0;
};

using ModesManagerPtr = std::shared_ptr<IModesManager>;

ModesManagerPtr createInstance(
    IModesManager::Config const& config,
    ModulesManager::ModulesManagerPtr modules_manager);

} // namespace ModesManager