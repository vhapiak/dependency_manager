#pragma once

#include <string>

#include "ModulesManager/RegisterInterface.hpp"

namespace ModesManager {

class IOnModeChanged {
public: // methods
    virtual ~IOnModeChanged() = default;

    virtual void onModeChanged(std::string const& mode) = 0;
};

}

DP_REGISTER_INTERFACE(ModesManager::IOnModeChanged*)