
#include <algorithm>

#include "Logger/Logger.hpp"
#include "ModesManager/IModesManager.hpp"
#include "ModesManager/IOnModeChanged.hpp"

namespace ModesManager {

class CModesManager : public IModesManager
{
public: // types
    struct ElementInstance
    {
        std::string name;
        ModulesManager::IModulesManager::InstanceId id;
        IOnModeChanged* accessor;
    };

public: // methods
    explicit CModesManager(IModesManager::Config const& config, ModulesManager::ModulesManagerPtr modules_manager)
        : m_modules_manager{modules_manager}
    {
        for (auto const& pair : config.elements)
        {
            auto const& name = pair.first;
            auto const& element = pair.second;
            m_elements_modules.emplace(name, element.module);
            for (auto const& mode : element.modes)
            {
                if (config.modes.count(mode) == 0)
                {
                    throw std::runtime_error{"Invalid mode name: " + mode};
                }
                m_elements_by_modes[mode].insert(name);
            }

            for (auto const& subpair : element.subelements)
            {
                auto const& subname = name + "." + subpair.first;
                auto const& subelement = subpair.second;
                m_elements_modules.emplace(subname, subelement.module);
                for (auto const& mode : subelement.modes)
                {
                    if (element.modes.count(mode) == 0)
                    {
                        throw std::runtime_error{
                            "Subelement must work in mode where parrent element is off" + mode};
                    }
                    m_elements_by_modes[mode].insert(subname);
                }
            }
        }
    }

    template <typename It>
    std::string print(It begin, It end)
    {
        std::string str;
        for (It it = begin; it != end; ++it)
        {
            str += (*it) + ", ";
        }
        return str;
    }

    void changeMode(std::string const& mode) override
    {
        LOG_DBG << "CModesManager::changeMode() " << mode;
        auto const it = m_elements_by_modes.find(mode);
        if (it == m_elements_by_modes.end())
        {
            throw std::runtime_error{"No such mode"};
        }
        auto& mode_elements = it->second;
        LOG_DBG << "Mode elements: " << print(mode_elements.begin(), mode_elements.end());

        std::set<std::string> active_elements_set;
        for (auto& element : m_active_elements)
        {
            active_elements_set.insert(element.name);
        }
        LOG_DBG << "Active elements: "
                << print(active_elements_set.begin(), active_elements_set.end());

        std::set<std::string> to_delete;
        std::set_difference(
            active_elements_set.begin(),
            active_elements_set.end(),
            mode_elements.begin(),
            mode_elements.end(),
            std::inserter(to_delete, to_delete.begin()));

        LOG_DBG << "Elements ro delete: " << print(to_delete.begin(), to_delete.end());

        for (auto it = m_active_elements.rbegin(); it != m_active_elements.rend();)
        {
            auto& element = *it;
            if (to_delete.count(element.name))
            {
                m_modules_manager->removeModuleInstance(element.id);
                auto forward_it = m_active_elements.erase(std::next(it).base());
                it = std::vector<ElementInstance>::reverse_iterator{forward_it};
            }
            else
            {
                ++it;
            }
        }

        for (auto& element : m_active_elements)
        {
            element.accessor->onModeChanged(mode);
        }

        std::vector<std::string> to_create;
        std::set_difference(
            mode_elements.begin(),
            mode_elements.end(),
            active_elements_set.begin(),
            active_elements_set.end(),
            std::back_inserter(to_create));

        LOG_DBG << "Elements to create: " << print(to_create.begin(), to_create.end());

        std::vector<ModulesManager::IModulesManager::InstanceData> modules_to_create;
        for (auto const& element : to_create)
        {
            auto& module = m_elements_modules.at(element);
            modules_to_create.push_back({module, element, {}});
        }

        auto order = m_modules_manager->sort(modules_to_create);
        for (auto* element : order)
        {
            auto id = m_modules_manager->createModuleInstance(element->module, element->name, {});
            auto accessor = m_modules_manager->getInterface<IOnModeChanged*>(
                element->module + "." + element->name + ".modes");
            m_active_elements.push_back({element->name, id, accessor});
        }
    }

    std::map<std::string, std::string> getActiveElements() const override
    {
        std::map<std::string, std::string> active_elements;
        for (auto& element : m_active_elements)
        {
            auto module = m_elements_modules.at(element.name);
            active_elements.emplace(element.name, module);
        }
        return active_elements;
    }

private:
    ModulesManager::ModulesManagerPtr m_modules_manager;

    std::map<std::string, std::string> m_elements_modules;
    std::map<std::string, std::set<std::string>> m_elements_by_modes;

    std::vector<ElementInstance> m_active_elements;
};

ModesManagerPtr createInstance(
    IModesManager::Config const& config,
    ModulesManager::ModulesManagerPtr modules_manager)
{
    return std::make_shared<CModesManager>(config, modules_manager);
}

} // namespace ModesManager